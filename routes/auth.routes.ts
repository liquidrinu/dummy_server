import express, { Express, Request, Response, Router } from 'express'

const router = Router()

// test routes
router.all("/*/", async (req: Request, res: Response) => {
  const data = await req.body
  res.status(200).json({
    url: req.url,
    headers: req.headers,
    description: "This is the received data, confirm if correct",
    data
  })
})

// login
router.post("/test/login", async (req: Request, res: Response) => {

  const { userName, passWord } = await req.body

  interface LoginCredentials {
    userName: string,
    passWord: string,
  }

  let user: LoginCredentials = {
    userName: process.env.USERNAME || "cat",
    passWord: process.env.PASSWORD || "meow",
  }

  if (userName === user.userName && passWord === user.passWord) {
    res.status(200).json({
      "username": user.userName,
      "status": true,
      "message": `Logged in as ${user.userName}`
    })
  } else {
    res.status(401).json({
      "username": user.userName,
      "status": false,
      "message": "Login was unsuccesfull!"
    })
  }
})

export default router
