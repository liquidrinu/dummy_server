# Dummy_server

- [Dummy_server](#dummy_server)
  - [Introduction](#introduction)
  - [Getting started](#getting-started)
  - [Using Docker](#using-docker)
  - [Endpoints](#endpoints)


## Introduction

A basic dummy server written in nodejs to test (frontend) API calls

This server is meant to test http calls. You can post with any route in the request, and 
only have to line up the host + port.

The response object will contain:
- the route in your request
- the headers
- the data object you send

## Getting started
Make sure you have nodejs and Yarn installed

```sh
# basic 
yarn install
yarn build
yarn dev
```

## Using Docker

```sh
docker build -t [app name] .
docker run -dp 12000:12000 [app name]
```

The json object to send (both key and values are pre-configured):

```javascript
POST to: http://localhost:12000/test/login

{
    "userName": "cat",
    "passWord": "meow"
}
```

```sh
# Optionally, you can also set a username and password in the .env file to override the default
USERNAME=myUser
PASSWORD=secret123
```

```sh
# build distributed folder
yarn build
```

## Endpoints
```code
GET   /
POST  /login
```