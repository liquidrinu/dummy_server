import express, { Express, Request, Response } from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import router from './router'

const app: Express = express()
const hostname = "localhost"
const port = 12000 //process.env.PORT || 8080 || 12000
const bodyParser = require('body-parser');
dotenv.config({ override: true })

const allowedOrigins = ['http://localhost:3000'];

const options: cors.CorsOptions = {
  origin: allowedOrigins
};

app.use(cors(options));

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.json())

app.use("/", router)

app.get('/', async (req: Request, res: Response) => {
  res.send({ "message": "This is a dummy server!" })
})

app.post('/', async (req: Request, res: Response) => {
  res.status(200).send(req.body)
})

app.listen(port, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})
